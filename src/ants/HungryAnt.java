package ants;

import core.Ant;
import core.AntColony;
import core.Bee;
import core.Place;

/**
 * An Ant hungry
 *
 * @author YOUR NAME HERE
 */
public class HungryAnt extends Ant {

	/**
	 * Creates a new HungryAnt Ant
	 */
	private int waitingTime;

	
	public HungryAnt() {
		super(4,1);
		this.waitingTime=0;

	}
	
	/**
	 * Get waintingtime of a hungryant
	 *
	 * @return waitingtime
	 *            return time of waitingtime
	 */
	public int getWaitingTime(){
		return waitingTime;
	}
	
	/**
	 * Set waintingtime of a hungryant
	 *
	 * @param time
	 *            time of waitingtime
	 */
	public void setWaintingTime(int time){
		this.waitingTime=time;
	}
	
	public void decreaseWaitingTime(){
		if(waitingTime-1<0)
			this.waitingTime=0;
		else
			this.waitingTime--;
	}

	@Override
	public void action (AntColony colony) {
		
		
			if(this.waitingTime==0){
				//System.out.println("I CAN EAT");
				Place hungryAntPlace = this.getPlace();
				Bee target = hungryAntPlace.getClosestBee(0, 0);
				if(target!=null){
					//System.out.println("I EAT");
					int damageTodo = target.getArmor();
					target.reduceArmor(damageTodo);
					this.waitingTime=3;
				}
			}
			else{
				//System.out.println("I CANTTT EAT");
				decreaseWaitingTime();
			}

	}
}