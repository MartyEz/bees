package ants;

import core.Bee;

public class ShortThrowerAnt extends ThrowerAnt {

	
	public  ShortThrowerAnt () {
		super(1,3);
		
	}
	
	public  ShortThrowerAnt (int armor, int foodCost) {
		super(armor,foodCost);
		
	}
	
	public Bee getTarget () {
		return place.getClosestBee(0, 2);
	}
}
