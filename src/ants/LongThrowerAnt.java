package ants;

import core.Bee;

public class LongThrowerAnt extends ThrowerAnt {
	
	public  LongThrowerAnt () {
		super(1,3);
		
	}
	
	public  LongThrowerAnt (int armor, int foodCost) {
		super(armor,foodCost);
		
	}
	
	public Bee getTarget () {
		return place.getClosestBee(0, 4);
	}

}
