package ants;

import core.Ant;
import core.AntColony;
import core.Bee;
import core.Damaging;
import core.Place;

/**
 * An Ant WAll
 *
 * @author YOUR NAME HERE
 */
public class FireAnt extends Ant implements Damaging{

	/**
	 * Creates a new FireAnt Ant
	 */
	
	private int damage;
	
	public FireAnt() {
		super(1,4);
		this.damage=3;
	}
	
	public void setDamage(int dmg){
		this.damage = dmg;
	}
	
	public int getDamage(){
		return this.damage;
	}
	
	public void reduceArmor(int dmg){
		int armor = this.getArmor();
		if(armor-dmg<=0){
			Place AntPlace = this.getPlace();
			Bee[] targets = AntPlace.getBees();
			for(int i =0; i<targets.length;i++){
				targets[i].reduceArmor(this.damage);
			}
		}
		super.reduceArmor(dmg);
	}
	

	@Override
	public void action (AntColony colony) {

	}
}