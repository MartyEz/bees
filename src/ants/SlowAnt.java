package ants;

import core.AntColony;
import core.Bee;

public class SlowAnt extends ThrowerAnt {
	
	public  SlowAnt () {
		super(1,4);
		
	}
	
	public  SlowAnt (int armor, int foodCost) {
		super(armor,foodCost);
		
	}
	
	public void action (AntColony colony) {
		Bee target = getTarget();
		if (target != null) {
			target.setSlowTime(1);;
		}
	}

}
