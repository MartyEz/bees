package ants;

import java.util.ArrayList;

import core.Ant;
import core.AntColony;

import core.Containing;
import core.Damaging;
import core.Place;
import core.QueenPlace;
import core.Unkillable;

public class QueenAnt extends Ant implements Unkillable{
	
	public static int nbIns=-1;
	private ArrayList<Ant> boosted;
	private boolean trueQueen=false;
	private QueenPlace qpr;
	
	
	public QueenAnt() {
		super(1,6);
		this.watersafe=true;
		boosted= new ArrayList<Ant>();
		//System.out.println("QUEEN CREATED");
		nbIns++;
		qpr = new QueenPlace();
		
		if(nbIns==1)
			trueQueen=true;
		
	}
	
	public void reduceArmor (int amount){
		
	}
	
	public void boost(Place a){
		
		Ant entranceAnt=null;
		
		if(a != null)
			entranceAnt = a.getAnt();
		
		if(entranceAnt != null){
			if(this.alreadyBoosted(entranceAnt)==false){
				if(entranceAnt instanceof Containing){
					if(((Containing) entranceAnt).getCInsect()!= null){
						if(((Containing) entranceAnt).getCInsect() instanceof Damaging){
							Ant son=(Ant) ((Containing) entranceAnt).getCInsect();
							int basicDamage = ((Damaging) son).getDamage();
							((Damaging) son).setDamage(basicDamage*2);
							boosted.add(entranceAnt);
							
						}
					}
				}
				else if(entranceAnt instanceof Damaging){
					int basicDamage = ((Damaging) entranceAnt).getDamage();
					((Damaging) entranceAnt).setDamage(basicDamage*2);
					boosted.add(entranceAnt);
				}
			}
		}
		
	}
	
	
	public void action(AntColony colony){
		
		if(trueQueen){
		
		qpr=colony.getQueenPlace();
		qpr.addIncPlace(this.place);
		colony.setQueenPlace(qpr);
		

		Place entrance = this.place.getEntrance();
		Place exit = this.place.getExit();
		
		boost(entrance);
		boost(exit);
		
		/*Ant entranceAnt=null;
		Ant exitAnt=null;
		if(entrance != null)
			entranceAnt = entrance.getAnt();
		if(exit != null)
			exitAnt = exit.getAnt();
		
		if(entranceAnt != null){
			if(this.alreadyBoosted(entranceAnt)==false){
				if(entranceAnt instanceof Containing){
					if(((Containing) entranceAnt).getCInsect()!= null){
						if(((Containing) entranceAnt).getCInsect() instanceof Damaging){
							Ant son=(Ant) ((Containing) entranceAnt).getCInsect();
							int basicDamage = ((Damaging) son).getDamage();
							((Damaging) son).setDamage(basicDamage*2);
							boosted.add(entranceAnt);
							
						}
					}
				}
				else if(entranceAnt instanceof Damaging){
					int basicDamage = ((Damaging) entranceAnt).getDamage();
					((Damaging) entranceAnt).setDamage(basicDamage*2);
					boosted.add(entranceAnt);
				}
			}
		}
		
		if(exitAnt != null){
			if(this.alreadyBoosted(exitAnt)==false){
				if(exitAnt instanceof Containing){
					if(((Containing) exitAnt).getCInsect()!= null){
						if(((Containing) exitAnt).getCInsect() instanceof Damaging){
							Ant son=(Ant) ((Containing) exitAnt).getCInsect();
							int basicDamage = ((Damaging) son).getDamage();
							((Damaging) son).setDamage(basicDamage*2);
							boosted.add(exitAnt);
						}
					}
				}
				else if(exitAnt instanceof Damaging){
					int basicDamage = ((Damaging) exitAnt).getDamage();
					((Damaging) exitAnt).setDamage(basicDamage*2);
					boosted.add(exitAnt);
				}
			}
		}*/
		}
		else
			this.leavePlace();
	}
	
	
	
	public boolean alreadyBoosted(Ant a){
		for(int i=0;i<boosted.size();i++){
			
			if(a==boosted.get(i))
				return true;
		}
		return false;
	}
	
}
