package ants;

import core.Ant;
import core.AntColony;
import core.Bee;
import core.Damaging;
import core.Place;

public class NinjaAnt extends Ant implements Damaging{
	
	
	private int damage;
	
	public NinjaAnt(){
		super(1,6);
		this.block=false;
		this.damage=1;
	}
	
	public int getDamage(){
		return this.damage;
	}
	
	public void setDamage(int dmg){
		this.damage=dmg;
	}
	
	

	@Override
	public void action(AntColony colony) {
		Place AntPlace = this.getPlace();
		Bee[] targets = AntPlace.getBees();
		for(int i =0; i<targets.length;i++){
			targets[i].reduceArmor(this.damage);
		}
	}

}
