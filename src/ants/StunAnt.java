package ants;



import core.AntColony;
import core.Bee;

public class StunAnt extends ThrowerAnt {
	
	public  StunAnt () {
		super(1,6);
		
	}
	
	public  StunAnt (int armor, int foodCost) {
		super(armor,foodCost);
		
	}
	
	public void action (AntColony colony) {
		Bee target = getTarget();
		if (target != null) {
			target.setStunTime(1);;
		}
	}

}
