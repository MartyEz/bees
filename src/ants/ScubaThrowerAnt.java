package ants;

import core.Damaging;

public class ScubaThrowerAnt extends ThrowerAnt {
	
	public ScubaThrowerAnt(){
		super(1,5);
		this.watersafe=true;
	}
	
	public ScubaThrowerAnt(int armor, int foodcost){
		super(armor,foodcost);
		this.watersafe=true;
	}

}
