package core;


public interface Containing {
	
	public boolean addCInsect(Insect ins);
	
	public boolean removeCInsect();
	
	public Insect getCInsect();

}
