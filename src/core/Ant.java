package core;

import ants.WallAnt;

/**
 * A class representing a basic Ant
 *
 * @author YOUR NAME HERE
 */
public abstract class Ant extends Insect {

	protected int foodCost; // the amount of food needed to make this ant

	/**
	 * Creates a new Ant, with a food cost of 0.
	 *
	 * @param armor
	 *            The armor of the ant.
	 */
	protected boolean block=true;
	
	public Ant (int armor) {
		super(armor, null);
		foodCost = 0;
	}
	
	public Ant (int armor, int foodCost){
		super(armor);
		this.foodCost=foodCost;
		
	}

	/**
	 * Returns the ant's food cost
	 *
	 * @return the ant's good cost
	 */
	public int getFoodCost () {
		return foodCost;
	}
	
	public boolean cantBlock(){
		return this.block;
	}

	/**
	 * Removes the ant from its current place
	 */
	@Override
	public void leavePlace () {
		System.out.println("I AM DEAD");

		place.removeInsect(this);
	}
}
