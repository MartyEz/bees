package core;

public class QueenPlace extends Place{

	
	private Place included;
	
	public QueenPlace(){
		super("QueenPlace");
		included=null;
		
	}
	
	public QueenPlace(String name) {
		super(name);
		
	}
	
	public QueenPlace(String name, Place exit) {
		super(name,exit);
	}
	
	public void addIncPlace(Place a){
		this.included=a;
	}
	
	public Place getIncPlace(){
		return this.included;
	}
	
	public Bee[] getBees(){
		Bee[] bee = super.getBees();
		Bee[] beeinc = null;
		Bee[] finalbee=null;
		int tmp=0;
		if(this.included != null){
			beeinc = this.included.getBees();
		}
		
		if(bee != null && beeinc !=null){
			finalbee = new Bee[bee.length + beeinc.length];
			for( int i=0;i<bee.length;i++){
				finalbee[i]= bee[i];
				tmp++;
			}
			for( int i=tmp+1;i<beeinc.length;i++){
				finalbee[i]= beeinc[i-tmp-1];
				
			}
			
		}
		else if(bee != null){
			finalbee = new Bee[bee.length];
			for( int i=0;i<bee.length;i++){
				finalbee[i]= bee[i];
				
			}
		}
		else if(beeinc !=null){
			finalbee = new Bee[beeinc.length];
			for( int i=0;i<beeinc.length;i++){
				finalbee[i]= beeinc[i];
				
			}
		}
		
		return finalbee;
	}
	


	
}
